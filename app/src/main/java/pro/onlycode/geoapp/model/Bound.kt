package pro.onlycode.geoapp.model

data class Bound(
    var lat: Double? = null,
    var lng: Double? = null
)