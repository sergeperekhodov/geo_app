package pro.onlycode.geoapp.model

data class Address(
    var country: String,
    var city: String,
    var county: String,
    var neighbourhood: String,
    var road: String,
    var house_number: String
)