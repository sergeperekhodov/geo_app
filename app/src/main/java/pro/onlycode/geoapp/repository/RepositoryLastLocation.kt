package pro.onlycode.geoapp.repository

import android.preference.PreferenceManager
import pro.onlycode.geoapp.App
import pro.onlycode.geoapp.Constants
import pro.onlycode.geoapp.model.Bound

class RepositoryLastLocation {
    private val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(App.appContext)
    private val editor = sharedPreferences.edit()

    val lastLocation: Bound?
        get() {
            if (sharedPreferences.getString(
                    Constants.locationLatTag,
                    ""
                ) != "" && sharedPreferences.getString(Constants.locationLngTag, "") != ""
            ) {
                val bound = Bound()
                bound.lat = java.lang.Double.valueOf(sharedPreferences.getString(Constants.locationLatTag, "")!!)
                bound.lng = java.lang.Double.valueOf(sharedPreferences.getString(Constants.locationLngTag, "")!!)
                return bound
            }

            return null
        }

    fun setLastLocation(lat: Double?, lng: Double?) {
        editor.putString(Constants.locationLatTag, lat.toString())
        editor.putString(Constants.locationLngTag, lng.toString())
        editor.commit()
    }
}
