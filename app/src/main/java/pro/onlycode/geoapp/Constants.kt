package pro.onlycode.geoapp

object Constants {
    var locationLatTag = "lat"
    var locationLngTag = "lng"

    var Latitude = 0.0
    var Longitude = 0.0
    var gpsEnable = false
}
