package pro.onlycode.geoapp.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_maps.*
import pro.onlycode.geoapp.App
import pro.onlycode.geoapp.Constants
import pro.onlycode.geoapp.R
import pro.onlycode.geoapp.model.Address
import pro.onlycode.geoapp.network.NetworkUtil
import pro.onlycode.geoapp.repository.RepositoryLastLocation
import java.util.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, LocationListener {

    private lateinit var mMap: GoogleMap
    private lateinit var compositeDisposable: CompositeDisposable
    private lateinit var locationManager: LocationManager
    private lateinit var repositoryLastLocation: RepositoryLastLocation
    private lateinit var address: Address

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        compositeDisposable = CompositeDisposable()
        locationManager = this.getSystemService(LOCATION_SERVICE) as LocationManager
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissionsGPS()
        }
        checkEnabled()
        get_current_address.setOnClickListener {
            getCurrentAddress()
        }


    }

    private fun getCurrentAddress() {
        showLoading()
        compositeDisposable.add(
            NetworkUtil.retrofit.getPlace(
                "json",
                RepositoryLastLocation().lastLocation?.lat.toString(),
                RepositoryLastLocation().lastLocation?.lng.toString()
            )

                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.address }
                .map {
                    address = Address(
                        it.country.toString(),
                        it.city.toString(),
                        it.county.toString(),
                        it.neighbourhood.toString(),
                        it.road.toString(),
                        it.houseNumber.toString()
                    )
                    return@map address
                }
                .subscribe(
                    {
                        hideLoading()
                        showAlert(
                            it.country + " " + it.city + " " + it.county + " " + it.neighbourhood
                                    + " " + it.road + " " + it.house_number
                        )

                    },
                    {
                        hideLoading()
                        it.printStackTrace()
                        showAlert(
                            resources.getString(R.string.error_text)
                        )
                    })
        )
    }

    private fun showLoading() {
        spinner.show()
    }

    private fun hideLoading() {
        spinner.hide()
    }


    private fun showAlert(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(resources.getString(R.string.current_location_text))
        builder.setMessage(message)
        builder.setNegativeButton(android.R.string.no) { _, _ ->
            Toast.makeText(
                applicationContext,
                android.R.string.no, Toast.LENGTH_SHORT
            ).show()
        }
        builder.show()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.clear()
        try {
            val currentLocationMarker =
                LatLng(
                    RepositoryLastLocation().lastLocation!!.lat!!, RepositoryLastLocation().lastLocation!!.lng!!
                )
            mMap.addMarker(MarkerOptions().position(currentLocationMarker).title(resources.getString(R.string.current_location_text)))
            mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocationMarker))

        } catch (e: Exception) {
            Log.d(
                resources.getString(R.string.error_text),
                resources.getString(R.string.error_getting_last_location_from_repo)
            )
            e.printStackTrace()
        }
    }

    private fun checkPermissionsGPS() {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .withListener(object : MultiplePermissionsListener {
                @SuppressLint("MissingPermission")
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        Log.d(
                            resources.getString(R.string.permission_text) + " ",
                            resources.getString(R.string.permission_granted)
                        )
                        locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            (1000 * 10).toLong(), 10f, this@MapsActivity
                        )
                        locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER, (1000 * 10).toLong(), 10f,
                            this@MapsActivity
                        )
                    }
                    if (report.isAnyPermissionPermanentlyDenied) {
                        // show alert dialog navigating to Settings
                        showSettingsDialog()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            })
            .check()

    }

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GOTO SETTINGS") { dialog, _ ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(
            "Cancel"
        ) { dialog, _ -> dialog.cancel() }
        builder.show()
    }

    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }


    override fun onLocationChanged(location: Location) {
        saveLocation(location)
        onMapReady(mMap)
        repositoryLastLocation = RepositoryLastLocation()
        repositoryLastLocation.setLastLocation(location.latitude, location.longitude)
    }


    @SuppressLint("ShowToast")
    override fun onProviderDisabled(provider: String) {
        checkEnabled()
        Toast.makeText(this, resources.getString(R.string.pls_turn_on_gps), Toast.LENGTH_LONG)

        Constants.gpsEnable = false
    }

    override fun onProviderEnabled(provider: String) {
        Constants.gpsEnable = true
        checkEnabled()
        if (ActivityCompat.checkSelfPermission(
                App.appContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                App.appContext,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        saveLocation(locationManager.getLastKnownLocation(provider))
    }

    private fun saveLocation(location: Location?) {
        if (location == null)
            return
//        highest accuracy
        if (location.provider == LocationManager.GPS_PROVIDER) {
            Log.d("GPS location ", formatLocation(location))
            repositoryLastLocation = RepositoryLastLocation()
            repositoryLastLocation.setLastLocation(location.latitude, location.longitude)

        } else if (location.provider == LocationManager.NETWORK_PROVIDER) {
//          high accuracy
            Log.d("NETWORK location ", formatLocation(location))
            repositoryLastLocation = RepositoryLastLocation()
            repositoryLastLocation.setLastLocation(location.latitude, location.longitude)
        }
//        passive provider has worst accuracy
    }

    private fun formatLocation(location: Location?): String {
        return if (location == null) "" else String.format(
            "Coordinates: lat = %1$.4f, lon = %2$.4f, time = %3\$tF %3\$tT",
            location.latitude, location.longitude, Date(
                location.time
            )
        )
    }

    private fun checkEnabled() {
        Log.d(
            "GPS ->", "Enabled: " + locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER)
        )
        Log.d(
            "NETWORK ->", "Enabled: " + locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        )
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
        if (provider == LocationManager.GPS_PROVIDER) {
            Log.d("Status: ", status.toString())
        } else if (provider == LocationManager.NETWORK_PROVIDER) {
            Log.d("Status: ", status.toString())
        }
    }

    override fun onPause() {
        super.onPause()
        locationManager.removeUpdates(this)
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            (1000 * 10).toLong(), 10f, this@MapsActivity
        )
        locationManager.requestLocationUpdates(
            LocationManager.NETWORK_PROVIDER, (1000 * 10).toLong(), 10f,
            this@MapsActivity
        )

    }
    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    fun View.show() {
        visibility = View.VISIBLE
    }

    fun View.hide() {
        visibility = View.GONE
    }
}
