package pro.onlycode.geoapp.network

import io.reactivex.Single
import pro.onlycode.geoapp.networkModel.Place
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RetrofitInterface {

    @GET("reverse.php")
    fun getPlace(
        @Query("format") format: String,
        @Query("lat") lat: String,
        @Query("lon") lon: String
    ): Single<Place>
}