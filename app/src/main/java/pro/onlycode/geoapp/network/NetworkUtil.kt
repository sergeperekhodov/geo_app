package pro.onlycode.geoapp.network

import io.reactivex.schedulers.Schedulers
import pro.onlycode.geoapp.BuildConfig
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object NetworkUtil {
    val retrofit: RetrofitInterface
        get() {

            val rxAdapter = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())

            return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addCallAdapterFactory(rxAdapter)
                .addConverterFactory(NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(RetrofitInterface::class.java)

        }


}

