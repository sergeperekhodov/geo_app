package pro.onlycode.geoapp

import android.app.Application
import android.content.Context

class App : Application() {

    override fun onCreate() {
        appContext = applicationContext
        super.onCreate()
    }

    companion object {
        lateinit var appContext: Context
            private set
    }
}